//
//  ViewController.h
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GalleryViewController.h"


@interface ViewController : UIViewController

@property (strong,nonatomic) GalleryViewController *gallery;

- (IBAction)buttonClicked:(id)sender;

@end
