//
//  ViewController.m
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    NSLog(@"ViewController did load");
    [super viewDidLoad];
    
    // Hide status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                              bundle:[NSBundle mainBundle]];
    
    _gallery = [storyboard instantiateViewControllerWithIdentifier:@"GalleryViewController"];
    [_gallery setModalPresentationStyle:UIModalPresentationFullScreen];
    [_gallery create];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)buttonClicked:(id)sender {
    [self showGallery:[sender tag]];
}


-(void) showGallery:(NSUInteger)index{
    NSLog(@"SHOW MODAL at index: %i", index);
    [self presentViewController:_gallery animated:YES completion:nil];
    [_gallery setPage:index];
}

@end
