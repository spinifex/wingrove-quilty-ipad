//
//  GalleryViewController.h
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController
<UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *scroll;
//@property (nonatomic) float *screenWidth;
//@property (nonatomic) float *screenHeight;

@property (nonatomic) NSNumber *pageIndex;

@property (strong, nonatomic) IBOutlet UIButton *homeBtn;
@property (strong, nonatomic) IBOutlet UIButton *prevBtn;
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;

@property (strong, nonatomic) IBOutlet UITextField *pageNumber;

- (void)setPage:(NSUInteger)index;
- (IBAction)onHome:(id)sender;
- (IBAction)onPrev:(id)sender;
- (IBAction)onNext:(id)sender;

-(void) create;
@end
