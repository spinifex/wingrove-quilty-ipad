//
//  AppDelegate.h
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
