//
//  GalleryViewController.m
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//
#define IsIOS8 (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1)


#import "GalleryViewController.h"

@interface GalleryViewController ()

@property NSInteger numberOfViews;
@property int stageWidth;
@property (strong, nonatomic) UIView *tint;
@end

@implementation GalleryViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setPage:(NSUInteger)index{
    [self setPageIndex:[[NSNumber alloc] initWithInt:index]];
    [_scroll setContentOffset:CGPointMake(index*_stageWidth, 0.0) animated:NO];
}

- (void)slidePage:(NSUInteger)index{
    [_scroll setContentOffset:CGPointMake(index*_stageWidth, 0.0) animated:YES];
}


- (IBAction)onHome:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onPrev:(id)sender {
    int newIndex = (int) _pageIndex.intValue;
    newIndex  = newIndex - 1;
    if(newIndex>=0){
        [self slidePage:newIndex];
    }
}

- (IBAction)onNext:(id)sender {
    int newIndex = (int) _pageIndex.intValue;
    newIndex = newIndex + 1;
    if(newIndex < _numberOfViews){
        [self slidePage:newIndex];
    }
}

-(void) create{
    _pageIndex = 0;
	_numberOfViews = 16;
    
    _tint = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 50)];
    _tint.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.75];
    
    [self.view addSubview:_tint];
    
    
    CGRect screenRect = [UIScreen mainScreen].bounds;
    //iOS screen size issues for <iOS8.0
    if (!IsIOS8){
        screenRect = [self getScreenFrameForCurrentOrientation];
    }
    
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    _stageWidth = screenWidth;
    
    CGFloat imageWidth = 458;
    CGFloat imageHeight = 614;
    
    CGFloat txtWidth = 443;
    CGFloat txtHeight = 613;

    _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    _scroll.pagingEnabled = YES;
    _scroll.contentSize = CGSizeMake(screenWidth * _numberOfViews, screenHeight);
    _scroll.showsHorizontalScrollIndicator = NO;
    _scroll.scrollsToTop = NO;
    _scroll.delegate = self;
    
    // add images to scroller
    for (int i = 0; i < _numberOfViews; i++) {
        
        CGFloat xOrigin = i * screenWidth;
        NSString *filename = [NSString stringWithFormat:@"img%i.jpg", (i + 1)];
        UIImage *img = [UIImage imageNamed:filename];
        UIImageView *image = [[UIImageView alloc] initWithImage:img];
        image.frame = CGRectMake(xOrigin + 41, 41, imageWidth, imageHeight);
        [_scroll addSubview:image];
        
 
        filename = [NSString stringWithFormat:@"txt%i.jpg", (i + 1)];
        UIImage *txtImg = [UIImage imageNamed:filename];
        UIImageView *txtImage = [[UIImageView alloc] initWithImage:txtImg];
        txtImage.frame = CGRectMake(xOrigin + 539, 41, txtWidth, txtHeight);
        [_scroll addSubview:txtImage];
        
    }
    [self.view addSubview:_scroll];
    [self.view sendSubviewToBack:_scroll];
    
}


- (CGRect)getScreenFrameForCurrentOrientation {
    return [self getScreenFrameForOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (CGRect)getScreenFrameForOrientation:(UIInterfaceOrientation)orientation {
    UIScreen *screen = [UIScreen mainScreen];
    CGRect fullScreenRect = screen.bounds;
    BOOL statusBarHidden = [UIApplication sharedApplication].statusBarHidden;
    
    //implicitly in Portrait orientation.
    if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
        CGRect temp = CGRectZero;
        temp.size.width = fullScreenRect.size.height;
        temp.size.height = fullScreenRect.size.width;
        fullScreenRect = temp;
    }
    
    if(!statusBarHidden){
        CGFloat statusBarHeight = 20;//Needs a better solution, FYI statusBarFrame reports wrong in some cases..
        fullScreenRect.size.height -= statusBarHeight;
    }
    
    return fullScreenRect;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    int page = ( (_scroll.contentOffset.x / _stageWidth) + 0.5f);
    [self setPageIndex:[[NSNumber alloc] initWithInt:page]];
}



-(void) setPageIndex:(NSNumber *) page{
    if(page.integerValue != _pageIndex.integerValue){
        if(page.integerValue == 0){
            _tint.frame = CGRectMake(500,700,150,50);
        }else if(page.integerValue == (_numberOfViews - 1)){
            _tint.frame = CGRectMake(900,700,150,50);
        }else{
            _tint.frame = CGRectMake(0,0,10,10);
        }
        
        NSString *sildeNumber = [NSString stringWithFormat:@"%i/%i", (page.integerValue+1), _numberOfViews];
        _pageNumber.text = sildeNumber;
        _pageIndex = [[NSNumber alloc] initWithInt:page.integerValue];
        
    }
}




@end
