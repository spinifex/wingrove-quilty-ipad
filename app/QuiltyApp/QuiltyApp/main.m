//
//  main.m
//  QuiltyApp
//
//  Created by Rob Shearing on 5/02/13.
//  Copyright (c) 2013 SpinifexGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
